;;; Nichlas Ørts Lisby
;;; 20165133
;;; nlisby16@student.aau.dk

;;; First part: Racket v7.4 for Windows 64 bit, written in Visual Studio Code for Windows
;;; Second part Racket v7.4 for Linux 64 bit, written in Visual Studio Code for Linux (My windows died on an automated update)

; My solution is pretty simple, and I dont have all the functionality I might have wanted (especially in regards to group (groupid Listofstudents)). 
; Groups however are implemented implicitly, as the places where we create groups, we keep the group number right along side the list of students.)
; My student-list is simply held as a list. There are different accessors and check predicates, to access and ensure it is a student.
; I haven't made as many predicates for the final task as I would've liked as I ran out of time.

#lang racket
(require racket/include)
(require "student-list.rkt")
(require "all-students.rkt")
(require "group-by-counting.rkt")
(require "group-by-random.rkt")
(require "group-by-sex-etnicity.rkt")
(require "group-by-random-predicate.rkt")

(define female-studentlist '( ("20203385" "Isabella Asmussen" "female" "Danish" 25)
                              ("20206655" "Isabella Hassan" "female" "Danish" 28)
                              ("20203059" "Astrid Esther Johnsen" "female" "Danish" 24)
                              ("20203849" "Caroline Brodersen" "female" "Danish" 23)))

(define (main)
      (isAStudent (get-student-by-id-as-student student-list "20202357"))
      (do-group-by-counting student-list) ;Kan også tage en gruppe count, altså hvor mange grupper, som parameter to.
      (do-Group-by-random student-list) ;Kan også tage en list of sizes in som parameter nummer 2
      (group-by-random student-list '(10 10 10 10 10 26 19 19 19 19 19 19 5 5))   
      (do-groupby-sex-ethnicity student-list 6) ; Kan også tage en gruppe count, altså hvor mange grupper, som parameter to.

      (do-Group-by-random-predicate female-studentlist all-female '(2 2))
      (do-Group-by-random-predicate student-list one-female-in-group '(100 100))
      (do-Group-by-random-predicate female-studentlist one-female-in-group '(1 1 1 1))
      ;(do-Group-by-random-predicate student-list all-female)
)
(main)

