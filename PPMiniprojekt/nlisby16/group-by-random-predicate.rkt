;;; Nichlas Ørts Lisby
;;; 20165133
;;; nlisby16@student.aau.dk

#lang racket
(require "assoc-list.rkt")
(require "student-list.rkt")
(require "group-by-random.rkt")
(require "printing.rkt")
(require "utilities.rkt")

(provide do-Group-by-random-predicate group-by-random-predicate for-all
; Predicates
all-female one-female-in-group )

; Simply an action to make it easy to do a group-by-random-predicate, prints
; Param1: student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
; Param2: predicate, a procedure which must be true for each created group
; Param3: list-of-sizes, default parameter, the sizes of the groups to create. A list of Numbers List(number). The sum of this must be equal to the length of student-list
(define (do-Group-by-random-predicate student-list predicate [list-of-sizes list-of-sizess])
(let ([random-grouping (group-by-random-predicate student-list list-of-sizes predicate)])
    (display(rec-format-group random-grouping "" student-list (number-of-groups-in-grouping random-grouping)))))


; Simply a wrapper for starting group-by-random-predicate with a shuffled list, maybe a bit redundant, but whatever
; Param1: student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
; Param2: list-of-sizes, default parameter, the sizes of the groups to create. A list of Numbers List(number). The sum of this must be equal to the length of student-list
; Param3: predicate, a procedure which must be true for each created group
(define (group-by-random-predicate student-list list-of-sizes predicate)
    (group-by-random-predicate-helper (shuffle student-list) list-of-sizes predicate))

; Appends to build a complete grouping based on a student-list and a list of sizes
; Builds one group at a time, tests for the predicate, and then appends to the result list
; If any of the built groups fails to build, it fails
; Param1: student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
; Param2: list-of-sizes, default parameter, the sizes of the groups to create. A list of Numbers List(number). The sum of this must be equal to the length of student-list
; Param3: predicate, a procedure which must be true for each created group
(define (group-by-random-predicate-helper student-list list-of-sizes predicate [groupidcounter 1])
    (if (null? student-list)
    '()
    (append (take-students-to-group-predicate-wrapper student-list (car list-of-sizes) groupidcounter predicate) 
            (group-by-random-predicate-helper (drop student-list (car list-of-sizes)) (cdr list-of-sizes) predicate (add1 groupidcounter)))))

; A wrapper to try to build a group by calling take-students-to-group and testing that group on a predicate
; Param1: student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
; Param2: amount, a number signifying the number of students to add to the current group
; Param3: groupid, a groupid signifying the current groupID
; Param4: predicate, a procedure which must be true for each created group
; Param5: res, the resulting group, on the form null or (groupid . studentnumber)
; Param6: times-to-try, a number signifying how many times to attempt to build a group
(define (take-students-to-group-predicate-wrapper student-list amount groupid predicate [res '()] [times-to-try 1000])
    (let ([chosen-group (take-students-to-group student-list amount groupid)])
        (if (predicate (get-single-group-as-students student-list chosen-group groupid)) ;here
        chosen-group
        (if (equal? times-to-try 0)
            (error "Failed to make randomgroups")
            (take-students-to-group-predicate-wrapper (shuffle student-list) amount groupid predicate '() (sub1 times-to-try))))))

;PREDICATE STUFF
; Test all members of a student-list against a predicate
; Param1: predicate, A procedure which must be true for all students in student-list
; Param2: student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
(define (for-all predicate student-list)
    (if (null? student-list) 
    #t
    (if(predicate (car student-list))
        (for-all predicate (cdr student-list))
        #f)))

;PREDICATE STUFF
; Test if one member of a student-list is true for a predicate
; Param1: predicate, A procedure which must be true for all students in student-list
; Param2: student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
(define (for-one predicate student-list)
    (if [list? student-list]
        (> (length (filter predicate student-list)) 0)
        (error "cannot call with non-list input" student-list)))

;Stuff I started for unique ages, but didnt finish
;;; (define (unique-ages student-list )
;;;     (if [list? student-list]
;;;         (> (length (filter (lambda (student) (equal? (get-student-age student) student-age) student-list))))
;;;         (error "not a list" student-list)))

;;; (define (unique-ages student-list)
;;;     (if []))

; Predicate to check if one female is in a group,  student-list meant to be a potential group
; Param1:  student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
(define (one-female-in-group student-list)
    (for-one (student-is-of-sex "female") student-list))

; Predicate to check if all female is in a student-list, student-list meant to be a potential group
; Param1:  student-list on the form: List(string:studentID string:name string:sex string:ethnicity number:age)
(define (all-female student-list)
    (for-all (student-is-of-sex "female") student-list))

; Default list of sizes
(define list-of-sizess '(10 10 10 10 10 20 20 20 20 20 20 20 5 5))
